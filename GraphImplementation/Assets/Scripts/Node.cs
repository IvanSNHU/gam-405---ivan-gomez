﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//node monobehaviour. implements a pathnode in the game world.
public class Node : MonoBehaviour
{
    [SerializeField] int connectionCount = 0;
    public PathNode myNode;

    //initialize the pathnode, pass the gameobject position. the constructor will autosubscribe to the graph.
    void Start()
    {
        myNode = new PathNode(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        //update this serialized field if we're in the editor. shows the number of links for this node.
        connectionCount = myNode.links.Count;
#endif
    }
}
