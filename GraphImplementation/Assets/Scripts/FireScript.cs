﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Node"))
        {
            PathNode temp = other.gameObject.GetComponent<Node>().myNode;
            temp.costScalar = 1000;
            Debug.Log("Cost of node " + temp.t + " is now 1000");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Node"))
        {
            PathNode temp = other.gameObject.GetComponent<Node>().myNode;
            temp.costScalar = 1;
            Debug.Log("Cost of node " + temp.t + " is now 1");
        }
    }
}
