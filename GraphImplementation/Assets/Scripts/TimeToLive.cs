﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToLive : MonoBehaviour
{
    [SerializeField] float ttl = 5;
    Rigidbody rb;
    float timeLived = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        timeLived += Time.deltaTime;
        if (timeLived >= ttl)
        {
            rb.AddForce(Vector3.up * 50);//, ForceMode.VelocityChange);
            Destroy(gameObject, 1.0f);
        }
    }
}
