﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public static PlayerScript instance;
    Rigidbody rb;
    Coroutine moving;

    void Awake()
    {
        //singleton things
        if (instance == null || instance == this)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MoveTo(PathNode node)
    {
        moving = StartCoroutine("Moving", node);
    }

    IEnumerator Moving(PathNode finalTarget)
    {
        Debug.Log("Coroutine heartbeat");
        PathNode currentTarget = GameManager.instance.myGraph.getClosestNode(transform.position);
        List<PathNode> myPath = GameManager.instance.myGraph.GeneratePath(currentTarget, finalTarget);
        for (int i = 0; i <= myPath.Count; i++)
        {
            if (currentTarget == default || currentTarget == null)
                StopAllCoroutines();
            Vector3 delta = currentTarget.t - transform.position;
            while (Vector3.SqrMagnitude(delta) > 1)
            {
                //Debug.Log("Moving");
                rb.MovePosition(transform.position + (Vector3.Normalize(currentTarget.t - transform.position) * Time.deltaTime * 4));
                delta = currentTarget.t - transform.position;
                yield return null;
            }
            if (currentTarget == finalTarget)
            {
                StopAllCoroutines();
                //StopCoroutine(moving);
            }
            else
            {
                currentTarget = myPath[i];
            }
        }
    }
}
