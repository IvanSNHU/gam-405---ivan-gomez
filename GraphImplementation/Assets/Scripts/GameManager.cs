﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject fire;
    public float maxNodeDistanceSquared = 10.0f;
    public static GameManager instance;
    public Graph myGraph = new Graph();
    [SerializeField] int graphCount = 0;
    [SerializeField] float gizmoRadius = 0.25f;
    float debugTimer = 0;
    float debugRate = 1.0f;

    void Awake()
    {
        //singleton things
        if (instance == null || instance == this)
        {
            instance = this;
            //initialize graph
            myGraph.nodes = new List<PathNode>();
        }
        else
        {
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        //update this serialized field if we're in the editor. shows the number of subscribed nodes
        graphCount = myGraph.nodes.Count;
#endif        

        //on left mouse click, raycast from the screen
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit temp;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out temp))
            {
                //print the collider we hit
                Debug.Log("Mouse click raycast: " + temp.collider.gameObject.name);
                try
                {
                    //find the closest node and print it. alert the player
                    PathNode closestNode = myGraph.getClosestNode(temp.point);
                    Debug.Log("Closest node to the raycast: " + closestNode.t);
                    PlayerScript.instance.MoveTo(closestNode);
                }
                catch (NullReferenceException) { }
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit temp;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out temp))
            {
                GameObject newGameObject = Instantiate(fire, temp.point + Vector3.up * 5.0f, Quaternion.identity);
            }
        }
    }

    private void OnDrawGizmos()
    {
        //as long as there are nodes, draw orbs for the nodes, draw lines for the links between nodes
        if (myGraph.nodes == null)
        {
            return;
        }
        foreach (var first in myGraph.nodes)
        {
            Gizmos.DrawSphere(first.t, gizmoRadius);

            if (first != null)
            {
                foreach (var second in first.links)
                {
                    Gizmos.DrawLine(first.t, second.t);
                }
            }
        }
    }
}

//define what a node is. a location and links. also has a link to the graph.
public class PathNode
{
    public Vector3 t;
    public float costScalar;
    public List<PathNode> links = new List<PathNode>();
    public static Graph g;

    //default constructor finds the graph and subscribes
    public PathNode(Vector3 t)
    {
        costScalar = 1;
        this.t = t;
        if (g == null)
        {
            g = GameManager.instance.myGraph;
        }
        g.reportIn(this);
    }
}

//define what a graph is. as list of nodes. links nodes when they subscribe, based on maxNodeDistance
public class Graph
{
    public List<PathNode> nodes;

    //nodes call this to be added to the graph
    public void reportIn(PathNode pn)
    {
        foreach (var item in nodes)
        {
            if (squareDistance(item.t, pn.t) < GameManager.instance.maxNodeDistanceSquared)
            {
                item.links.Add(pn);
                pn.links.Add(item);
            }
        }
        nodes.Add(pn);
    }

    //gets closest node based off of collisions. could be changed to just iterate through all nodes if the graph doesn't get too big. breaks if glosest node is too far away as is.
    public PathNode getClosestNode(Vector3 pos)
    {
        Collider returnMe = null;
        float minDistance = float.MaxValue;
        float altDistance = 0f;
        foreach (var hit in Physics.OverlapSphere(pos, 20.0f))
        {
            if (hit.CompareTag("Node"))
            {
                altDistance = squareDistance(pos, hit.transform.position);
                if (returnMe == null)
                {
                    returnMe = hit;
                    minDistance = altDistance;
                }
                else if (altDistance < minDistance)
                {
                    returnMe = hit;
                    minDistance = altDistance;
                }
            }
        }
        return returnMe.gameObject.GetComponent<Node>().myNode;
    }

    //returns squared distance from a to b
    public static float squareDistance(Vector3 a, Vector3 b)
    {
        float returnMe = 0;
        Vector3 temp = b - a;
        returnMe = (temp.x * temp.x) + (temp.y * temp.y) + (temp.z * temp.z);
        return returnMe;
    }

    //A* pathfinding
    public List<PathNode> GeneratePath(PathNode start, PathNode end)
    {
        //placeholder for the path we will return
        List<PathNode> returnMe = new List<PathNode>();

        PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>();
        List<PathNode> visited = new List<PathNode>();
        //to - from paths dictionary
        Dictionary<PathNode, PathNode> paths = new Dictionary<PathNode, PathNode>();
        Dictionary<PathNode, float> costs = new Dictionary<PathNode, float>();

        //start at the beginning node
        PathNode focus = start;
        while (focus != end && focus != default)
        {
            foreach (var link in focus.links)
            {
                if (!visited.Contains(link))
                {
                    float previousCost = 0;
                    costs.TryGetValue(focus, out previousCost);
                    float currentCost = previousCost + NodeScore(focus, link, end);
                    //keep track of how we got to that node. add as TO, FROM
                    if (!paths.ContainsKey(link))
                    {
                        paths.Add(link, focus);
                        costs.Add(link, currentCost);
                    }
                    //add all links to the frontier based on score
                    frontier.Enqueue(link, currentCost);
                }
            }
            visited.Add(focus);
            focus = frontier.Dequeue();
            //returnMe.Add(focus);
        }
        //if we got to the end, start building the path. builds from the end to the beginning.
        if (focus == end && paths.Count > 0)
        {
            returnMe.Insert(0, focus);
            returnMe.Insert(0, paths[focus]);
            while (paths[focus] != start)
            {
                focus = returnMe[0];
                returnMe.Insert(0, paths[focus]);
            }
        }
        //else just return the start point.
        else
        {
            returnMe.Add(start);
        }

        return returnMe;
    }

    //low is good
    float NodeScore(PathNode currentPosition, PathNode focus, PathNode end)
    {
        return NodeHueristic(currentPosition, focus, end) + NodeCost(currentPosition, focus);
    }

    float NodeHueristic(PathNode currentPosition, PathNode focus, PathNode end)
    {
        //high dot is good
        float returnMe = 0;
        returnMe = Vector3.Dot(end.t - currentPosition.t, focus.t - currentPosition.t);
        //returnMe = Mathf.Abs(returnMe);
        //Debug.Log("Dot value: " + returnMe);
        return returnMe * -100;
    }

    float NodeCost(PathNode currentPosition, PathNode focus)
    {
        //high cost is bad
        float returnMe = Vector3.SqrMagnitude(focus.t - currentPosition.t) * focus.costScalar;
        //Debug.Log("Cost value: " + returnMe);
        return returnMe;
    }
}

//define a priority queue. adds values into a list and positions them based on an int. returns and dequeues the Lowest value when requested.
public class PriorityQueue<DataType>
{
    //our internal structure
    LinkedList<Element> buffer;

    //this enqueue just exists so that you don't have to build an element first if you don't want to
    public void Enqueue(DataType data, float priority)
    {
        Enqueue(new Element(data, priority));
    }

    //add a value to the correct spot in our internal structure
    public void Enqueue(Element element)
    {
        LinkedListNode<Element> i = buffer.First;
        bool done = false;
        while (!done)
        {
            if (buffer.Count < 1)
            {
                buffer.AddFirst(element);
                done = !done;
            }
            else if (element.priority < i.Value.priority)
            {
                buffer.AddBefore(i, element);
                done = !done;
            }
            else if (i == buffer.Last)
            {
                buffer.AddAfter(i, element);
                done = !done;
            }
            else
            {
                i = i.Next;
            }
        }
    }

    //pop off numero uno
    public DataType Dequeue()
    {
        if (buffer.Count > 0)
        {
            Element returnMe = buffer.First.Value;
            buffer.RemoveFirst();
            return returnMe.data;
        }
        else
            return default(DataType);
    }

    //constructor. just makes the list.
    public PriorityQueue()
    {
        buffer = new LinkedList<Element>();
    }

    //dataType that our buffer is built up from. just holds the data and a priority. lets us use a linked list instead of a dictionary or something.
    public class Element
    {
        public DataType data;
        public float priority;
        public Element(DataType data, float priority)
        {
            this.data = data;
            this.priority = priority;
        }
    }
}