﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoogeScript : MonoBehaviour
{
    public static StoogeScript instance;

    private void Awake()
    {
        //singleton
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GoTo(Vector3 point)
    {
        var path = GridManager.instance.pathfind(transform.position, point);
        for (int i = 0; i < path.Count; i++)
        {
            Debug.Log(path[i]);
        }
    }
}
