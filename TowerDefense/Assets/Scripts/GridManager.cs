﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public static GridManager instance;
    [SerializeField] GameObject gridSpace;
    [SerializeField] Vector3 start;
    [SerializeField] Vector3 offset;
    [SerializeField] Vector2 dimensions;
    List<GridSpace> spaces = new List<GridSpace>();

    private void Awake()
    {
        //singleton
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //create the gridspaces and their connections
        for (int x = 0; x < dimensions.x; x++)
        {
            for (int y = 0; y < dimensions.y; y++)
            {
                Vector3 tempV3 = start;
                tempV3.x += offset.x * x;
                tempV3.z += offset.z * y;
                GameObject tempObject = Instantiate(gridSpace, tempV3, Quaternion.identity, gameObject.transform);
                GridSpace tempGrid = tempObject.GetComponent<GridSpace>();
                tempGrid.mySpot = new Vector2(x, y);
                foreach (var space in spaces)
                {
                    if (space != tempGrid)
                    {
                        if (squareDistance(space.mySpot, tempGrid.mySpot) < 1.1f)
                        {
                            space.connections.Add(tempGrid);
                            tempGrid.connections.Add(space);
                        }
                    }
                }
            }
        }
        Debug.Log("Grid count: " + spaces.Count);
    }

    float squareDistance(Vector2 a, Vector2 b)
    {
        //squared distance between Vector2 points
        return Mathf.Pow(b.x - a.x, 2) + Mathf.Pow(b.y - a.y, 2);
    }

    GridSpace closestGrid(Vector2 point)
    {
        //return the closest gridSpace to a given Vector2 point
        float smallestDistance = float.MaxValue;
        GridSpace closestGrid = new GridSpace();
        foreach (var space in spaces)
        {
            if (squareDistance(space.mySpot, point) < smallestDistance)
            {
                smallestDistance = squareDistance(space.mySpot, point);
                closestGrid = space;
            }
        }
        return closestGrid;
    }

    public List<GridSpace> pathfind(GridSpace a, GridSpace b)
    {
        //calculate the best path between two points
        List<GridSpace> returnMe = new List<GridSpace>();
        var focus = a;
        var target = b;

        //List<GridSpace> frontier = new List<GridSpace>();
        List<GridSpace> visited = new List<GridSpace>();
        GridSpacePriorityQueue2 frontier = new GridSpacePriorityQueue2();
        //paths are TO, FROM
        Dictionary<GridSpace, GridSpace> paths = new Dictionary<GridSpace, GridSpace>();
        Dictionary<GridSpace, float> costs = new Dictionary<GridSpace, float>();

        if (a == b)
        {
            returnMe.Add(a);
        }
        else
        {
            costs.Add(a, a.cost);
            frontier.Enqueue(a, costs[a]);
            visited.Add(a);
            while (frontier.Count() != 0 && focus != b)
            {
                foreach (var connection in focus.connections)
                {
                    if (!visited.Contains(connection))
                    {
                        try
                        {
                            frontier.Enqueue(connection, costs[focus] + connection.cost);
                            paths.Add(connection, focus);
                            costs.Add(connection, costs[focus] + connection.cost);
                        }
                        catch
                        {
                            //nothing
                        }
                    }
                }
                focus = frontier.Dequeue();
            }
            if (focus == b)
            {
                while (focus != a)
                {
                    returnMe.Insert(0, focus);
                    focus = paths[focus];
                }
                returnMe.Insert(0, a);
            }
            else
            {
                returnMe.Add(a);
            }
        }
        return returnMe;
    }

    public List<GridSpace> pathfind(Vector2 a, GridSpace b)
    {
        //override in case I don't have both gridspaces
        return pathfind(closestGrid(a), b);
    }

    public List<GridSpace> pathfind(GridSpace a, Vector2 b)
    {
        //override in case I don't have both gridspaces
        return pathfind(a, closestGrid(b));
    }

    public List<GridSpace> pathfind(Vector2 a, Vector2 b)
    {
        //override in case I don't have both gridspaces
        return pathfind(closestGrid(a), closestGrid(b));
    }

    public List<GridSpace> pathfind(Vector3 a, Vector3 b)
    {
        Vector2 newA = new Vector2(a.x, a.z);
        Vector2 newB = new Vector2(b.x, b.z);
        return pathfind(newA, newB);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Register(GridSpace newGrid)
    {
        //used by the spaces to register with this gridManager
        spaces.Add(newGrid);
    }
}

public class GridSpacePriorityQueue
{
    //Priority queue that returns the lowest cost item. doesn't actually store things sorted
    Dictionary<GridSpace, float> internalData = new Dictionary<GridSpace, float>();

    public void Enqueue(GridSpace a, float b)
    {
        internalData.Add(a, b);
    }

    public GridSpace Dequeue()
    {
        float lowestCost = float.MaxValue;
        GridSpace cheapestGrid = new GridSpace();
        foreach (var item in internalData)
        {
            if (item.Value < lowestCost)
            {
                lowestCost = item.Value;
                cheapestGrid = item.Key;
            }
        }
        internalData.Remove(cheapestGrid);
        return cheapestGrid;
    }

    public int Count()
    {
        return internalData.Count;
    }
}

public class GridSpacePriorityQueue2
{
    //a priority queue with better performance (i think)

    class Element
    {
        public GridSpace gridSpace;
        public float cost;

        public Element(GridSpace a, float cost)
        {
            this.gridSpace = a;
            this.cost = cost;
        }
    }

    List<Element> internalData = new List<Element>();

    public void Enqueue(GridSpace a, float b)
    {
        bool done = false;
        for (int i = 0; i < internalData.Count; i++)
        {
            if (b < internalData[i].cost)
            {
                internalData.Insert(i, new Element(a, b));
                done = true;
                break;
            }
        }
        if (!done)
        {
            internalData.Add(new Element(a, b));
        }
    }

    public GridSpace Dequeue()
    {
        GridSpace returnMe = internalData[0].gridSpace;
        internalData.RemoveAt(0);
        return returnMe;
    }

    public int Count()
    {
        return internalData.Count;
    }
}