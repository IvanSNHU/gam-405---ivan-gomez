﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    GridManager gm;

    private void Awake()
    {
        //singleton
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gm = GridManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        //highlight the grid under the cursor
        RaycastHit[] deets;
        deets = screenToWorldResults();
        foreach (var deet in deets)
        {
            if (deet.collider.CompareTag("GridSpace"))
            {
                deet.collider.gameObject.GetComponent<GridSpace>().highlight();
                break;
            }
        }
    }

    public void CatchAllPointerDown(BaseEventData data)
    {
        //Gives me a debug message of what grid you clicked on
        RaycastHit[] deets;
        deets = screenToWorldResults();
        foreach (var deet in deets)
        {
            if (deet.collider.CompareTag("GridSpace"))
            {
                Vector2 tempSpot = deet.collider.gameObject.GetComponent<GridSpace>().mySpot;
                Debug.Log("Hit GridSpace " + tempSpot);
                StoogeScript.instance.GoTo(tempSpot);
                break;
            }
        }
    }

    RaycastHit[] screenToWorldResults()
    {
        //shoot a ray from the camera to the world based on the mouse position, and return all colliders hit
        RaycastHit[] deets;
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            deets = Physics.RaycastAll(ray, 100.0f);
        }
        return deets;
    }
}
