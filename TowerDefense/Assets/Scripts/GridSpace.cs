﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSpace : MonoBehaviour
{
    public Vector2 mySpot;
    MeshRenderer mr;
    string colorString = "_BaseColor";
    Color defaultColor;
    [HideInInspector] public List<GridSpace> connections = new List<GridSpace>();
    public bool passable = true;
    public float cost = 1.0f;

    private void Awake()
    {
        GridManager.instance.Register(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        mr = GetComponent<MeshRenderer>();
        defaultColor = mr.material.GetColor(colorString);
        //GridManager.instance.Register(this);
    }

    // Update is called once per frame
    void Update()
    {
        Color temp = mr.material.color;
        //temp.a -= Time.deltaTime;
        if (temp.a > 0)
        {
            temp.a = Mathf.Max(0, temp.a - Time.deltaTime);
            mr.material.SetColor(colorString, temp);
        }
    }

    public void highlight()
    {
        //Debug.Log("Highlight called");
        Color temp = defaultColor;
        temp.a = 0.5f;
        mr.material.SetColor(colorString, temp);
    }

    public override bool Equals(object other)
    {
        // If parameter is null return false.
        if (other == null)
        {
            return false;
        }

        // If parameter cannot be cast return false.
        GridSpace temp = other as GridSpace;
        if ((System.Object)temp == null)
        {
            return false;
        }

        return this.mySpot == temp.mySpot;
    }

    public override string ToString()
    {
        return mySpot.ToString();
    }

    public override int GetHashCode()
    {
        return mySpot.GetHashCode();
    }
}
