﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BattleBot", menuName = "AI/BattleBot")]
public class BattleBot : BaseAI
{
    //first thought was to do scorched earth and drop one bomb on every tile, every turn, except where I end up each turn. i'm assuming you can't move and bomb that much though.
    /*
     priorities, high to low:
     evade bombs,
     deliver the gem,
     pick up the gem,
     if someone else has the gem defend the goal,
     explore the unknown
         */
    public bool verbose = false;
    knowledgeMap myMap = new knowledgeMap();
    int2 myPos = new int2(0, 0);

    //keep track of our world information
    class knowledgeMap
    {
        Dictionary<int2, verboseSensorData> tiles = new Dictionary<int2, verboseSensorData>();
        public bool goalSighted = false;
        public int2 goalPosition = new int2();
        public bool diamondSighted = false;
        public int2 diamondPosition = new int2();
        public bool verbose = false;

        //return the number of tiles that have been added
        public int tileCount()
        {
            return tiles.Count;
        }

        //everywhere we've been to
        HashSet<int2> beenTo = new HashSet<int2>();

        //update a tile
        public void update(int x, int y, verboseSensorData data)
        {
            update(new int2(x, y), data);
        }

        //update a tile
        public void update(int2 coord, verboseSensorData data)
        {
            if (tiles.ContainsKey(coord))
                tiles[coord] = data;
            else
                tiles.Add(coord, data);
            if (data.diamond)
            {
                diamondPosition = coord;
                diamondSighted = true;
                Debug.Log("Diamond sighted!");
            }
            if (data.goal)
            {
                goalPosition = coord;
                goalSighted = true;
                Debug.Log("Goal sighted!");
            }
        }

        //update all touching tiles
        public void update(int2 myPos, sensorPack data)
        {
            update(myPos, data.curr);
            beenTo.Add(myPos);
            update(myPos + new int2(0, 1), data.up);
            update(myPos + new int2(0, -1), data.down);
            update(myPos + new int2(-1, 0), data.left);
            update(myPos + new int2(1, 0), data.right);
        }

        //fetch info on a tile
        public verboseSensorData fetch(int x, int y)
        {
            return fetch(new int2(x, y));
        }

        //fetch info on a tile
        public verboseSensorData fetch(int2 coord)
        {
            return tiles[coord];
        }

        //check if a coord is contained in our map
        public bool contains(int2 coord)
        {
            if (tiles.ContainsKey(coord))
                return true;
            else
                return false;
        }

        //check if a coord is contained in our map
        public bool contains(int x, int y)
        {
            return contains(new int2(x, y));
        }

        //find the furthest point that we haven't visited
        public int2 furthestUnknown(int2 pos)
        {
            //Debug.Log("Current pos: " + pos);
            int2 furthest = new int2(pos);
            float delta = 0;
            foreach (var tile in tiles)
            {
                if (tile.Value.clear && !beenTo.Contains(tile.Key))
                {
                    if (sqrDistance(tile.Key, pos) > delta)
                    {
                        delta = sqrDistance(tile.Key, pos);
                        furthest = tile.Key;
                    }
                }
            }
            //Debug.Log("furthestUnknown" + furthest);
            return furthest;
        }

        //pythagorean distance between two points
        public float distance(int2 a, int2 b)
        {
            return Mathf.Sqrt(sqrDistance(a, b));
        }

        //squared pythagorean distance between two points
        public float sqrDistance(int2 a, int2 b)
        {
            int2 c = b - a;
            return c.x * c.x + c.y * c.y;
        }

        //generate a path from A to B
        public List<GameManager.Direction> travel(int2 start, int2 end)
        {
            //Debug.Log("from: " + start + " to: " + end);
            List<GameManager.Direction> returnMe = new List<GameManager.Direction>();
            List<int2> nodes = new List<int2>();
            if (sqrDistance(start, end) <= 1.00f)
            {
                //Debug.Log("Early out");
                nodes.Add(end);
                returnMe = nodesToCardinals(start, nodes);
                //foreach (var node in nodes)
                //{
                //    //Debug.Log("node " + node.x + " " + node.y);
                //}
                //foreach (var cardinal in returnMe)
                //{
                //    //Debug.Log("cardinal: " + cardinalToText(cardinal));
                //}                
                return returnMe;
            }
            //return returnMe;

            int2 focus = new int2(start);
            //toGO
            HashSet<int2> frontier = new HashSet<int2>();
            //beenTo
            HashSet<int2> visited = new HashSet<int2>();
            //to, from
            Dictionary<int2, int2> paths = new Dictionary<int2, int2>();
            Dictionary<int2, float> costs = new Dictionary<int2, float>();
            int i = 1;
            while (focus != end)// && frontier.Count != visited.Count)
            {
                if (frontier.Count > 0 && frontier.Count == visited.Count)
                {
                    //Debug.Log("Unpathable");
                    break;
                }
                updateFrontier(frontier, focus);
                visited.Add(focus);
                foreach (var item in frontier)
                {
                    if (!paths.ContainsKey(item))
                    {
                        paths.Add(item, focus);
                        costs.Add(item, i);
                    }
                }
                focus = lowestUnvisitedValue(costs, visited);
                i++;
            }
            if (focus == end && paths.Count > 0)
            {
                nodes.Insert(0, focus);
                nodes.Insert(0, paths[focus]);
                {
                    while (paths[focus] != start)
                    {
                        focus = nodes[0];
                        nodes.Insert(0, paths[focus]);
                    }
                }
            }
            else
            {
                nodes.Insert(0, start);
            }
            foreach (var node in nodes)
            {
                //Debug.Log("node " + node);
            }
            returnMe = nodesToCardinals(start, nodes);
            foreach (var cardinal in returnMe)
            {
                //Debug.Log("cardinal: " + cardinalToText(cardinal));
            }

            return returnMe;
        }

        //convert a direction to a string
        string cardinalToText(GameManager.Direction cardinal)
        {
            switch (cardinal)
            {
                case GameManager.Direction.Current:
                    return "Current";
                case GameManager.Direction.Down:
                    return "Down";
                case GameManager.Direction.Left:
                    return "Left";
                case GameManager.Direction.Right:
                    return "Right";
                case GameManager.Direction.Up:
                    return "Up";
                default:
                    return "";
            }
        }

        //convert from nodes to a list of directions
        List<GameManager.Direction> nodesToCardinals(int2 start, List<int2> nodes)
        {
            List<GameManager.Direction> returnMe = new List<GameManager.Direction>();
            returnMe.Add(nodeToCardinal(start, nodes[0]));
            for (int i = 0; i < nodes.Count - 1; i++)
            {
                returnMe.Add(nodeToCardinal(nodes[i], nodes[i + 1]));
            }
            return returnMe;
        }

        //convert a delta between tiles to a direction
        GameManager.Direction nodeToCardinal(int2 a, int2 b)
        {
            if (b - a == new int2(0, 1))
            {
                return GameManager.Direction.Up;
            }
            else if (b - a == new int2(0, -1))
            {
                return GameManager.Direction.Down;
            }
            else if (b - a == new int2(-1, 0))
            {
                return GameManager.Direction.Left;
            }
            else if (b - a == new int2(1, 0))
            {
                return GameManager.Direction.Right;
            }
            else
                return GameManager.Direction.Current;
        }

        //return the tile with the lowest cost, NOT on the visited list
        int2 lowestUnvisitedValue(Dictionary<int2, float> input, HashSet<int2> visited)
        {
            int2 returnMe = new int2();
            float lowest = float.MaxValue;
            foreach (var item in input)
            {
                if (item.Value < lowest && !visited.Contains(item.Key))
                {
                    lowest = item.Value;
                    returnMe = item.Key;
                }
            }
            //Debug.Log("lowestUnvisitedValue" + returnMe);
            return returnMe;
        }

        //add new tiles to the frontier
        //hash sets don't care about duplicates
        public void updateFrontier(HashSet<int2> frontier, int2 pos)
        {
            if (contains(pos) && fetch(pos).clear)
            {
                frontier.Add(pos);
            }
            if (contains(pos + new int2(0, 1)) && fetch(pos + new int2(0, 1)).clear)
            {
                frontier.Add(pos + new int2(0, 1));
            }
            if (contains(pos + new int2(0, -1)) && fetch(pos + new int2(0, -1)).clear)
            {
                frontier.Add(pos + new int2(0, -1));
            }
            if (contains(pos + new int2(1, 0)) && fetch(pos + new int2(1, 0)).clear)
            {
                frontier.Add(pos + new int2(1, 0));
            }
            if (contains(pos + new int2(-1, 0)) && fetch(pos + new int2(-1, 0)).clear)
            {
                frontier.Add(pos + new int2(-1, 0));
            }
        }
    }

    //like a vec2, but with ints
    class int2
    {
        public int x, y;

        public int2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ")";
        }

        public int2(int2 input) : this(input.x, input.y) { }

        public int2() : this(0, 0) { }

        public static int2 operator +(int2 a, int2 b)
        {
            return new int2(a.x + b.x, a.y + b.y);
        }

        public static int2 operator -(int2 a, int2 b)
        {
            return new int2(a.x - b.x, a.y - b.y);
        }

        public static bool operator ==(int2 a, int2 b)
        {
            if (a.x == b.x && a.y == b.y)
                return true;
            return false;
        }

        public static bool operator !=(int2 a, int2 b)
        {
            if (a.x != b.x || a.y != b.y)
                return true;
            return false;
        }

        //squish the ints into shorts, turn those into one int, return
        public override int GetHashCode()
        {
            short firstHalf = (short)x;
            short secondHalf = (short)y;
            int returnMe = 0;
            returnMe += firstHalf * (2 ^ 16);
            returnMe += secondHalf;
            return returnMe;
        }

        public override bool Equals(object obj)
        {
            //THIS SECTION IS NOT PURELY MY CODE. NEVER OVERRIDEN "EQUALS(OBJECT OBJ)" BEFORE.

            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast return false.
            int2 p = obj as int2;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (x == p.x) && (y == p.y);

            //END DISCLAIMER
        }
    }

    //compilation of "verbose" sensor data, tied to cardinal directions
    class sensorPack
    {
        public verboseSensorData curr, down, left, right, up;

        public sensorPack()
        {
            curr = new verboseSensorData();
            down = new verboseSensorData();
            left = new verboseSensorData();
            right = new verboseSensorData();
            up = new verboseSensorData();
        }

        public sensorPack(sensorPack data)
        {
            curr = data.curr;
            down = data.down;
            left = data.left;
            right = data.right;
            up = data.up;
        }

        public sensorPack(GameManager.SensorData curr, GameManager.SensorData down, GameManager.SensorData left, GameManager.SensorData right, GameManager.SensorData up)
        {
            this.curr = new verboseSensorData(curr);
            this.down = new verboseSensorData(down);
            this.left = new verboseSensorData(left);
            this.right = new verboseSensorData(right);
            this.up = new verboseSensorData(up);
        }
    }

    //bit to boolean conversion of sensordata
    class verboseSensorData
    {
        public bool bomb, clear, diamond, enemy, goal, offgrid, wall;

        public verboseSensorData()
        {
            bomb = false;
            clear = false;
            diamond = false;
            enemy = false;
            goal = false;
            offgrid = false;
            wall = false;
        }

        public verboseSensorData(verboseSensorData data)
        {
            bomb = data.bomb;
            clear = data.clear;
            diamond = data.diamond;
            enemy = data.enemy;
            goal = data.goal;
            offgrid = data.offgrid;
            wall = data.wall;
        }

        public verboseSensorData(GameManager.SensorData data) : this()
        {
            if ((data & GameManager.SensorData.Bomb) != 0)
            {
                bomb = true;
            }
            if ((data & GameManager.SensorData.Clear) != 0)
            {
                clear = true;
            }
            if ((data & GameManager.SensorData.Diamond) != 0)
            {
                diamond = true;
            }
            if ((data & GameManager.SensorData.Enemy) != 0)
            {
                enemy = true;
            }
            if ((data & GameManager.SensorData.Goal) != 0)
            {
                goal = true;
            }
            if ((data & GameManager.SensorData.OffGrid) != 0)
            {
                offgrid = true;
            }
            if ((data & GameManager.SensorData.Wall) != 0)
            {
                wall = true;
            }
        }
    }

    //the ONE function that gets called externally. get sensor data, process it, build an action
    public override CombatantAction GetAction(ref List<GameManager.Direction> aMoves, ref int aBombTime)
    {
        sensorPack newSensorPack = new sensorPack();
        {
            GameManager.SensorData curr = UseSensor(GameManager.Direction.Current);
            //printSensorData(GameManager.Direction.Current, curr);
            newSensorPack.curr = new verboseSensorData(curr);

            GameManager.SensorData down = UseSensor(GameManager.Direction.Down);
            //printSensorData(GameManager.Direction.Down, down);
            newSensorPack.down = new verboseSensorData(down);

            GameManager.SensorData left = UseSensor(GameManager.Direction.Left);
            //printSensorData(GameManager.Direction.Left, left);
            newSensorPack.left = new verboseSensorData(left);

            GameManager.SensorData right = UseSensor(GameManager.Direction.Right);
            //printSensorData(GameManager.Direction.Right, right);
            newSensorPack.right = new verboseSensorData(right);

            GameManager.SensorData up = UseSensor(GameManager.Direction.Up);
            //printSensorData(GameManager.Direction.Up, up);
            newSensorPack.up = new verboseSensorData(up);
        }
        processSensorPack(myPos, newSensorPack);
        return calculateCombatantAction(ref aMoves, ref aBombTime);
    }

    //send off the sensor data
    void processSensorPack(int2 pos, sensorPack newSensorPack)
    {
        myMap.update(pos, newSensorPack);
    }

    /*
     priorities, high to low:
     evade bombs,
     deliver the gem,
     pick up the gem,
     if someone else has the gem defend the goal,
     explore the unknown
         */
    //decide what the heck to do
    CombatantAction calculateCombatantAction(ref List<GameManager.Direction> aMoves, ref int aBombTime)
    {
        //aMoves = new List<GameManager.Direction>();
        aBombTime = 2;
        //Debug.Log("Tiles: " + myMap.tileCount());
        if (nearbyBomb(myPos))
        {
            if (verbose)
                Debug.Log("Logic: nearby bomb, evade bombs");
            aMoves = myMap.travel(myPos, myMap.furthestUnknown(myPos));
            updatePos(aMoves);
            return CombatantAction.Move;
        }
        else if (myPos == myMap.diamondPosition && myMap.goalSighted)
        {
            if (verbose)
                Debug.Log("Logic: have gem, deposit gem");
            aMoves = myMap.travel(myPos, myMap.goalPosition);
            updatePos(aMoves);
            return CombatantAction.Move;
        }
        else if (myPos == myMap.diamondPosition && !myMap.goalSighted)
        {
            if (verbose)
                Debug.Log("Logic: have gem, find goal");
            aMoves = myMap.travel(myPos, myMap.furthestUnknown(myPos));
            updatePos(aMoves);
            return CombatantAction.Move;
        }
        else if (myMap.diamondSighted && !myMap.fetch(myMap.diamondPosition).enemy)
        {
            if (verbose)
                Debug.Log("Logic: unprotected gem in sight, get gem");
            aMoves = myMap.travel(myPos, myMap.diamondPosition);
            updatePos(aMoves);
            return CombatantAction.Move;
        }
        else if (myMap.diamondSighted && myMap.fetch(myMap.diamondPosition).enemy && myMap.goalSighted && !myMap.fetch(myPos).goal)
        {
            if (verbose)
                Debug.Log("Logic: enemy has gem, I'm not on goal, go to goal");
            aMoves = myMap.travel(myPos, myMap.goalPosition);
            updatePos(aMoves);
            return CombatantAction.Move;
        }
        else if (nearbyEnemy(myPos))
        {
            if (verbose)
                Debug.Log("Logic: nearby enemy, drop bomb");
            return CombatantAction.DropBomb;
        }
        else if (myMap.diamondSighted && myMap.fetch(myMap.diamondPosition).enemy && myMap.goalSighted && myMap.fetch(myPos).goal)
        {
            if (verbose)
                Debug.Log("Logic: enemy has gem, I'm on goal, drop bomb");
            return CombatantAction.DropBomb;
        }
        else
        {
            if (verbose)
                Debug.Log("Logic: nothing to worry about, explore");
            aMoves = myMap.travel(myPos, myMap.furthestUnknown(myPos));
            updatePos(aMoves);
            return CombatantAction.Move;
        }
    }

    //update my current pos based one my move request
    public void updatePos(List<GameManager.Direction> moves)
    {
        foreach (var item in moves)
        {
            myPos += directionToInt2(item);
        }
    }

    //convert a direction to an int2
    private int2 directionToInt2(GameManager.Direction item)
    {
        switch (item)
        {
            case GameManager.Direction.Current:
                return new int2(0, 0);
            case GameManager.Direction.Down:
                return new int2(0, -1);
            case GameManager.Direction.Left:
                return new int2(-1, 0);
            case GameManager.Direction.Right:
                return new int2(1, 0);
            case GameManager.Direction.Up:
                return new int2(0, 1);
            default:
                return new int2(0, 0);
        }
    }

    //return true if in explosion radius of a bomb. should only be used on current position, and only after scanning
    bool nearbyBomb(int2 pos)
    {
        bool returnMe = false;
        if (myMap.fetch(pos).bomb)
            returnMe = true;
        else if (myMap.contains(pos + new int2(0, 1)) && myMap.fetch(pos + new int2(0, 1)).bomb)
            returnMe = true;
        else if (myMap.contains(pos + new int2(0, -1)) && myMap.fetch(pos + new int2(0, -1)).bomb)
            returnMe = true;
        else if (myMap.contains(pos + new int2(-1, 0)) && myMap.fetch(pos + new int2(-1, 0)).bomb)
            returnMe = true;
        else if (myMap.contains(pos + new int2(1, 0)) && myMap.fetch(pos + new int2(1, 0)).bomb)
            returnMe = true;
        return returnMe;
    }

    //return true if an enemy would be in explosion radius
    bool nearbyEnemy(int2 pos)
    {
        bool returnMe = false;
        if (myMap.fetch(pos).enemy)
            returnMe = true;
        else if (myMap.contains(pos + new int2(0, 1)) && myMap.fetch(pos + new int2(0, 1)).enemy)
            returnMe = true;
        else if (myMap.contains(pos + new int2(0, -1)) && myMap.fetch(pos + new int2(0, -1)).enemy)
            returnMe = true;
        else if (myMap.contains(pos + new int2(-1, 0)) && myMap.fetch(pos + new int2(-1, 0)).enemy)
            returnMe = true;
        else if (myMap.contains(pos + new int2(1, 0)) && myMap.fetch(pos + new int2(1, 0)).enemy)
            returnMe = true;
        return returnMe;
    }

    //print the sensor data from one scan
    void printSensorData(GameManager.Direction dir, GameManager.SensorData data)
    {
        // & GameManager.SensorData.Clear
        switch (dir)
        {
            case GameManager.Direction.Current:
                if (verbose)
                    Debug.Log("Current");
                break;
            case GameManager.Direction.Down:
                if (verbose)
                    Debug.Log("Down");
                break;
            case GameManager.Direction.Left:
                if (verbose)
                    Debug.Log("Left");
                break;
            case GameManager.Direction.Right:
                if (verbose)
                    Debug.Log("Right");
                break;
            case GameManager.Direction.Up:
                if (verbose)
                    Debug.Log("Up");
                break;
        }
        if ((data & GameManager.SensorData.Bomb) != 0)
        {
            if (verbose)
                Debug.Log("Bomb");
        }
        if ((data & GameManager.SensorData.Clear) != 0)
        {
            if (verbose)
                Debug.Log("Clear");
        }
        if ((data & GameManager.SensorData.Diamond) != 0)
        {
            if (verbose)
                Debug.Log("Diamond");
        }
        if ((data & GameManager.SensorData.Enemy) != 0)
        {
            if (verbose)
                Debug.Log("Enemy");
        }
        if ((data & GameManager.SensorData.Goal) != 0)
        {
            if (verbose)
                Debug.Log("Goal");
        }
        if ((data & GameManager.SensorData.OffGrid) != 0)
        {
            if (verbose)
                Debug.Log("OffGrid");
        }
        if ((data & GameManager.SensorData.Wall) != 0)
        {
            if (verbose)
                Debug.Log("Wall");
        }
    }
}
