﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyMeshy : MonoBehaviour
{
    [SerializeField] Vector3[] myVertices;// = new Vector3[4];
    [SerializeField] Vector2[] myUV;
    [SerializeField] int[] myTriangles;
    Mesh myMesh;// = new Mesh();

    // Start is called before the first frame update
    void Awake()
    {
        myMesh = new Mesh();
        GetComponent<MeshFilter>().mesh = myMesh;
        myMesh.vertices = myVertices;
        myMesh.uv = myUV;
        myMesh.triangles = myTriangles;
    }

    private void Start()
    {
        //GameManager.instance.CheckIn(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, 0.1f);
    }
}
