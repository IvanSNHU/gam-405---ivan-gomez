﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareMesh : MonoBehaviour
{
    LineRenderer line;
    public Vector3[] verts;
    public Vector3[] worldVerts;
    [SerializeField] Vector2 heightAndWidth = new Vector2(10, 10);

    // Start is called before the first frame update
    void Awake()
    {
        line = GetComponent<LineRenderer>();
        //turn the height and width into a square
        verts = new Vector3[] { new Vector3(-heightAndWidth.x / 2, 0, -heightAndWidth.y / 2), new Vector3(heightAndWidth.x / 2, 0, -heightAndWidth.y / 2),
            new Vector3(heightAndWidth.x / 2, 0, heightAndWidth.y / 2), new Vector3(-heightAndWidth.x / 2, 0, heightAndWidth.y / 2) };
        //turn the square into world positions
        UpdateWorldVerts();
    }

    private void Start()
    {
        GameManager.instance.CheckIn(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void UpdateWorldVerts()
    {
        worldVerts = new Vector3[verts.Length];
        for (int i = 0; i < verts.Length; i++)
        {
            worldVerts[i] = verts[i] + transform.position;
        }
        line.positionCount = verts.Length;
        line.SetPositions(worldVerts);
    }
}
