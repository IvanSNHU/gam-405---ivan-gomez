﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class AIState
{
    public virtual void EnterState() { }
    public virtual void Update() { }
    public virtual void ExitState() { }
}