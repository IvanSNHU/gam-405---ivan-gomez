﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public GameObject targetMark;

    enum Behavior
    {
        Nothing,
        Fetch,
        Target
    }
    Behavior currentBehavior = default;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Down(BaseEventData data)
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
        {
            if (Input.GetMouseButton(0))
            {
                Debug.Log("Mouse hit at " + hitInfo.point);

                if (hitInfo.collider.CompareTag("TargetMark"))
                {
                    switch (currentBehavior)
                    {
                        case Behavior.Fetch:
                            DogScript.instance.FetchCommand(hitInfo.collider.gameObject);
                            break;
                        case Behavior.Target:
                            DogScript.instance.TargetCommand(hitInfo.collider.gameObject);
                            break;
                        default:
                            //do nothing
                            break;
                    }
                }
            }

            else if (Input.GetMouseButtonDown(1))
            {
                Vector3 targetPosition = new Vector3(
                    Mathf.Clamp(hitInfo.point.x, -19.0f, 19.0f),
                    0.5f,
                    Mathf.Clamp(hitInfo.point.z, -19.0f, 19.0f));
                Instantiate(targetMark, targetPosition, Quaternion.identity);
            }
        }
    }

    private void OnMouseDown()
    {
        Debug.Log("Mouse Down");
    }

    public void FetchChange(bool data)
    {
        if (data)
        {
            currentBehavior = Behavior.Fetch;
        }
        else
        {
            currentBehavior = default;
        }
        Debug.Log("Fetch is " + data);
    }

    public void TargetChange(bool data)
    {
        if (data)
        {
            currentBehavior = Behavior.Target;
        }
        else
        {
            currentBehavior = default;
        }
        Debug.Log("Target is " + data);
    }
}
