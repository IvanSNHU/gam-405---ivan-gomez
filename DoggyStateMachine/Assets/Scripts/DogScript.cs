﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogScript : MonoBehaviour
{
    [SerializeField] float movementScalar = 1.0f;
    public static DogScript instance;
    Rigidbody rb;

    enum AIStates
    {
        idle,
        walk,
        fetch,
        target
    }
    AIStates currentState = AIStates.idle;
    GameObject targetObject;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case (AIStates.idle):
                //do noting
                break;
            case (AIStates.walk):
                break;
            case (AIStates.fetch):
                if (targetObject.transform.parent == transform && Vector3.SqrMagnitude(transform.position) < 4.0f)
                {
                    currentState = AIStates.idle;
                    targetObject.transform.parent = null;
                    targetObject.GetComponent<Rigidbody>().isKinematic = false;
                }
                else if (targetObject.transform.parent == transform)
                {
                    WalkTo(Vector3.zero);
                }
                else if (Vector3.SqrMagnitude(targetObject.transform.position - transform.position) < 3.0f)
                {
                    targetObject.GetComponent<Rigidbody>().isKinematic = true;
                    targetObject.transform.SetParent(transform);
                }
                else
                {
                    WalkTo(targetObject.transform.position);
                }
                break;
            case (AIStates.target):
                if (Vector3.SqrMagnitude(targetObject.transform.position - transform.position) < 4.0f)
                {
                    currentState = AIStates.idle;
                }
                else
                {
                    WalkTo(targetObject.transform.position);
                }
                break;
            default:
                Debug.Log("Default switch in DogScript. Shouldn't happen.");
                break;
        }
    }

    void WalkTo(Vector3 position)
    {
        transform.LookAt(new Vector3(position.x, 0.5f, position.z), Vector3.up);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        rb.MovePosition(transform.position + transform.forward * Time.deltaTime * movementScalar);
        //transform.position += transform.forward * Time.deltaTime;
    }

    public void FetchCommand(GameObject targetObject)
    {
        Debug.Log("Fetch " + targetObject.name);
        if (currentState == AIStates.idle)
        {
            currentState = AIStates.fetch;
            this.targetObject = targetObject;
        }
    }

    public void TargetCommand(GameObject targetObject)
    {
        Debug.Log("Target " + targetObject.name);
        if (currentState == AIStates.idle)
        {
            currentState = AIStates.target;
            this.targetObject = targetObject;
        }

    }
}
